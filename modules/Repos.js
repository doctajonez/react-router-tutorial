import React from 'react';

import NavLink from './NavLink'

class Repos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: props.params.username || '',
            repo: props.params.repoName || ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRepoChange = this.handleRepoChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const path = `/repos/${this.state.username}/${this.state.repo}`;
        this.context.router.push(path);
        console.log(path);
    }

    handleUsernameChange(e) {
        this.setState({
            username: e.target.value,
            repo: this.state.repo
        });
    }

    handleRepoChange(e) {
        this.setState({
            username: this.state.username,
            repo: e.target.value
        });
    }

    render() {
        return (
            <div>
                <h2>Repos</h2>

                <ul>
                    <li><NavLink to="/repos/rackt/react-router">React router</NavLink></li>
                    <li><NavLink to="/repos/facebook/react">React</NavLink></li>

                    <li>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" placeholder="userName" value={this.state.username} onChange={this.handleUsernameChange} /> /
                            <input type="text" placeholder="repo" value={this.state.repo} onChange={this.handleRepoChange} />
                            <button type="submit">Go</button>
                        </form>
                    </li>
                </ul>

                { this.props.children }
            </div>
        );
    }
}

Repos.contextTypes = {
    router: React.PropTypes.object
};

export default Repos;