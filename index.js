import React from 'react'
import { Router, Route, hashHistory, browserHistory, IndexRoute } from 'react-router'
import App from './modules/app'
import About from './modules/About'
import Repos from './modules/Repos'
import Repo from './modules/Repo'
import Home from './modules/home'
import { render } from 'react-dom'

render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home} />
            <Route path="/about" component={About} />
            <Route path="/repos" component={Repos}>
                <Route path="/repos/:username/:repoName" component={Repo} />
            </Route>
        </Route>
    </Router>
), document.getElementById('app'));